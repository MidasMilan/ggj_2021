using Mirror;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class NetworkCameraBehaviour : NetworkBehaviour
{
    private Camera thisCam;

    // Start is called before the first frame update
    void OnEnable()
    {
        thisCam = GetComponent<Camera>();
        thisCam.enabled = isLocalPlayer;
    }
}
