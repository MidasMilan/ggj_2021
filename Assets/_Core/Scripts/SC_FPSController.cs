using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class SC_FPSController : NetworkBehaviour
{
    [SerializeField] private float walkingSpeed = 7.5f;
    [SerializeField] private float runningSpeed = 11.5f;
    [SerializeField] private float jumpSpeed = 8.0f;
    [SerializeField] private float gravity = 20.0f;
    [SerializeField] private Camera playerCamera;
    [SerializeField] private float lookSpeed = 2.0f;
    [SerializeField] private float lookXLimit = 45.0f;

    [SerializeField] private Transform pickupFollowTarget;
    [SerializeField] private float maxPickupDistance = 1.5f;
    [SerializeField] private float throwStrength;
    [SerializeField] private float invisibleLength = 2.0f;

    [SerializeField] private MeshRenderer beerMesh;
    [SerializeField] private MeshRenderer playerMesh;
    [SerializeField] private MeshRenderer neusMesh;

    private GameManager gameManagerPointer;

    private GameObject pickedUpObject;

    private CharacterController characterController;
    private Vector3 moveDirection = Vector3.zero;
    private float rotationX = 0;
    private bool isRunning;
    private float invisibleCounter;

    [HideInInspector]
    public bool canMove = true;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        isRunning = false;
        invisibleCounter = 0.0f;

        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        playerCamera.enabled = isLocalPlayer;
        if (isLocalPlayer) playerCamera.GetComponent<AudioListener>().enabled = true;
        beerMesh.enabled = false;

        gameManagerPointer = GameObject.FindGameObjectsWithTag("GameManager")[0].GetComponent<GameManager>();// I believe god forgives even murder under the right circumstances
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (!isLocalPlayer) return;

        // We are grounded, so recalculate move direction based on axes
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);
        // Press Left Shift to run
        float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Vertical") : 0;
        float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Horizontal") : 0;
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * curSpeedX) + (right * curSpeedY);

        if (Input.GetButtonDown("Jump") && canMove && characterController.isGrounded)
        {
            moveDirection.y = jumpSpeed;
        }
        else
        {
            moveDirection.y = movementDirectionY;
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        if (!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);

        // Player and Camera rotation
        if (canMove)
        {
            rotationX += -Input.GetAxis("Mouse Y") * lookSpeed;
            rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
            playerCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
            transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (pickedUpObject != null)
            {
                if (pickedUpObject.CompareTag("Bear"))
                {
                    CmdThrowBear(netId, pickedUpObject, pickupFollowTarget.position, pickupFollowTarget.rotation, playerCamera.transform.forward * throwStrength);
                }
                pickedUpObject = null;
            }
            else
            {
                // Bit shift the index of the layer (8) to get a bit mask
                int layerMask = 1 << 8;

                // This would cast rays only against colliders in layer 8.
                // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
                layerMask = ~layerMask;
                RaycastHit hit;
                if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, maxPickupDistance, layerMask))
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    GameObject hitGO = hit.collider.gameObject;
                    if (hitGO.CompareTag("Bear"))
                    {
                        invisibleCounter = invisibleLength;
                        beerMesh.enabled = true;
                        CmdPickupBear(netId, hitGO);
                        pickedUpObject = hitGO;
                    }
                }
                else
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            // Bit shift the index of the layer (8) to get a bit mask
            int layerMask = 1 << 8;

            // This would cast rays only against colliders in layer 8.
            // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
            layerMask = ~layerMask;
            RaycastHit hit;
            if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, maxPickupDistance * 1.5f, layerMask))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                GameObject hitGO = hit.collider.gameObject;
                if (hitGO.CompareTag("Deur"))
                {
                    hitGO.GetComponent<DoorBehaviour>().CmdTriggerDoor();
                }
            }
            else
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            }
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            CmdStartGame();
        }

        if (invisibleCounter > 0.0f && !playerMesh.enabled)
        {
            invisibleCounter -= Time.deltaTime;
            if (invisibleCounter <= 0.0f)
            {
                CmdSetPlayerVisible(netId, true);
            }
        }
    }

    [Command]
    private void CmdPickupBear(uint ownerNetID, GameObject bear)
    {
        RpcSetPlayerVisible(ownerNetID, false);
        bear.GetComponent<BearBehaviour>().PickUp();
    }

    [Command]
    private void CmdThrowBear(uint ownerNetID, GameObject bear, Vector3 position, Quaternion rotation, Vector3 throwForce)
    {
        RpcSetPlayerVisible(ownerNetID, true);
        RpcSetPlayerBearVisible(ownerNetID, false);
        bear.GetComponent<BearBehaviour>().Throw(position, rotation, throwForce);
    }

    [Command]
    void CmdSetPlayerVisible(uint playerNetID, bool value)
    {
        RpcSetPlayerVisible(playerNetID, value);
        RpcSetPlayerBearVisible(playerNetID, value);
    }

    [ClientRpc]
    void RpcSetPlayerVisible(uint playerNetID, bool value)
    {
        if (playerNetID != netId) return;

        playerMesh.enabled = value;
        neusMesh.enabled = value;
    }

    [ClientRpc]
    void RpcSetPlayerBearVisible(uint ownerNetID, bool value)
    {
        beerMesh.enabled = value;
    }

    [Command]
    void CmdStartGame()
    {
        print("Trying start game!");
        gameManagerPointer.StartGame();
    }
}