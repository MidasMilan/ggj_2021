using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class BearBehaviour : NetworkBehaviour
{
    private Rigidbody thisRB;

    [ClientRpc]
    public void PickUp()
    {
        gameObject.SetActive(false);
    }

    [ClientRpc]
    public void Throw(Vector3 startPos, Quaternion startRot, Vector3 force)
    {
        transform.position = startPos;
        transform.rotation = startRot;
        gameObject.SetActive(true);
        thisRB.AddForce(force);
    }

    private void Start()
    {
        thisRB = GetComponent<Rigidbody>();
    }
}
