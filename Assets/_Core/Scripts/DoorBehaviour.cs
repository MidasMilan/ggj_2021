using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DoorBehaviour : NetworkBehaviour
{
    NetworkAnimator netAnim;

    private void Start()
    {
        netAnim = GetComponent<NetworkAnimator>();
    }

    [Command(ignoreAuthority = true)]
    public void CmdTriggerDoor()
    {
        netAnim.animator.SetTrigger("InteractDoor");
        netAnim.SetTrigger("InteractDoor");
    }
}
