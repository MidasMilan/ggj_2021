using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : NetworkBehaviour
{
    [SerializeField] private Transform[] bearSpawnPoints;
    [SerializeField] private NetworkManager networkManagerPointer;

    private bool gameStarted;

    private void Start()
    {
        gameStarted = false;
    }

    public void StartGame()
    {
        print($"Start game called! isServer: {isServer}");
        if (gameStarted || !isServer) return;

        Transform spawnPoint = bearSpawnPoints[Random.Range(0, bearSpawnPoints.Length)];
        GameObject bear = Instantiate(networkManagerPointer.spawnPrefabs[0], spawnPoint.position, spawnPoint.rotation);
        NetworkServer.Spawn(bear);
    }
}
